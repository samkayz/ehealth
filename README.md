# Installation Guide #

This is the Breakdown of the installation guilde.

### What yo need to run the Application? ###

* Python 3+
* Django 3+
* djoser
* MySql 5+

### How do I get set up? ###

* install python 3 from https://python.org
* Clone project: git clone https://samkayz@bitbucket.org/samkayz/ehealth.git
* cd to path to clone ehealth directory; delete env folder
* Type "python3 -m venv env" for iOs/Linux base and "python -m venv env" for window base OS to create virtual environment
* cd/path to ehealth/env/bin/activate to activate the virtual environment
* cd back to ehealth folder NOTE: After activating the virtual environment, the env should be in bracket i.e something like this (env) samkayzs-MacBook-Air:ehealth samkayz$ 

### Database configuration ###

* Install Mysql 5+
* You can install Xampp, Wamp or any MySql client
* Create database 
* Open settings.py from ehealth django project and look for DATABASE Settings and configure it

### Running of the Application ###

* Type "pip install -r requirements.txt" (Window Base) or "pip3 install -r requirements.txt"(iOs/Linux) to install all the packages needed for the project.
* After successfully installing everything,
* Type "python manage.py makemigrations and python manage.py migrate to migrate all the database (Window Base)
* Type "python3 manage.py makemigrations and python3 manage.py migrate to migrate all the database (iOs/Linux base)
* run "python manage.py runserver/python3 manage.py runserver
* The server will be started at url http://127.0.0.1:8000

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact