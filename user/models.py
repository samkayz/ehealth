from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.sessions.models import Session
from django.conf import settings


class User(AbstractUser):
    title = models.CharField(max_length=100)
    email = models.EmailField(verbose_name='email', unique=True)
    phone = models.CharField(verbose_name='phone', max_length=100)
    user_role = models.IntegerField()
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name', 'phone', 'last_name']
    USERNAME_FIELD = 'email'

    class Meta:
        db_table = 'user'

    def get_username(self):
        return self


class UserSession(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    session = models.OneToOneField(Session, on_delete=models.CASCADE)

    class Meta:
        db_table = 'user_session'


class MedicalInfo(models.Model):
    user_id = models.IntegerField()
    first_name = models.CharField(max_length=255)
    last_name =models.CharField(max_length=255)
    email = models.EmailField()
    phone = models.CharField(max_length=50)
    dob = models.CharField(max_length=50)
    blood_type = models.CharField(max_length=50)
    m_status = models.CharField(max_length=50)
    address = models.TextField()
    sex = models.CharField(max_length=50)
    medical = models.CharField(max_length=50)

    class Meta:
        db_table = 'medical_info'

