from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model, authenticate, login as dj_login, logout as s_logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
UserModel = get_user_model()
from . models import *
from django.http import HttpResponse
from .fusioncharts import FusionCharts
from collections import OrderedDict


# Login Function Than Handle Both the Login of User and Medical Practional
def signin(request):
    if request.user.is_authenticated:
        return redirect('dashboard')
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']

        user = authenticate(email=email, password=password)

        if user is not None:
            dj_login(request, user)
            return redirect('dashboard')
        else:
            messages.error(request, 'Invalid Credentials')
            return redirect('signin')
    else:
        return render(request, 'login.html')



# SignUp Function Than Handle the SignUp of User
def signup(request):
    if request.user.is_authenticated:
        return redirect('dashboard')
    if request.method == "POST":
        title = request.POST['title']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        phone_no = request.POST['phone']
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']

        if password1 == password2:
            if UserModel.objects.filter(email=email).exists():
                messages.error(request, 'Email Taken')
                return redirect('signup')
            elif UserModel.objects.filter(phone=phone_no).exists():
                messages.error(request, 'Mobile Number Used')
                return redirect('signup')
            else:
                user = UserModel.objects.create_user(username=email,
                                                     password=password1,
                                                     email=email,
                                                     first_name=first_name,
                                                     last_name=last_name,
                                                     phone=phone_no,
                                                     title=title,
                                                     user_role="1"
                                                     )
                user.save()
                user = authenticate(email=email, password=password1)
                dj_login(request, user)
                return redirect('dashboard')
    else:
        return render(request, 'signup.html')


# SignUp Function Than Handle the SignUp of Medical Practional
def doc_signup(request):
    if request.method == "POST":
        title = request.POST['title']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        phone_no = request.POST['phone']
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']

        if password1 == password2:
            if UserModel.objects.filter(email=email).exists():
                messages.error(request, 'Email Taken')
                return redirect('doc_signup')
            elif UserModel.objects.filter(phone=phone_no).exists():
                messages.error(request, 'Mobile Number Used')
                return redirect('doc_signup')
            else:
                user = UserModel.objects.create_user(username=email,
                                                     password=password1,
                                                     email=email,
                                                     first_name=first_name,
                                                     last_name=last_name,
                                                     phone=phone_no,
                                                     title=title,
                                                     user_role="2"
                                                     )
                user.save()
                user = authenticate(email=email, password=password1)
                dj_login(request, user)
                return redirect('dashboard')
    else:
        return render(request, 'doc_signup.html')


# Dashboard Function Than Handle What the User see after successfully logged in
@login_required(login_url='signin')
def dashboard(request):
    total_doc = UserModel.objects.filter(user_role=2).count()
    total_user = UserModel.objects.filter(user_role=1).count()
    med_info = MedicalInfo.objects.filter().count()
    all_staff = UserModel.objects.filter(user_role=2).count()
    return render(request, 'dashboard.html', {'total_doc': total_doc, 'total_user': total_user, 'med_info': med_info,
    'all_staff': all_staff})


# Function that handle User Medical information of user
@login_required(login_url='signin')
def medical_info(request):
    first_name = request.user.first_name
    last_name = request.user.last_name
    email = request.user.email
    phone = request.user.phone
    c_id = request.user.id
    if request.method == "POST":
        dob = request.POST['dob']
        blood_type = request.POST['blood_type']
        m_status = request.POST['m_status']
        address = request.POST['address']
        sex = request.POST['sex']
        medical = request.POST['medical']
        if MedicalInfo.objects.filter(user_id=c_id).exists():
            MedicalInfo.objects.filter(user_id=c_id).update(dob=dob, blood_type=blood_type, m_status=m_status, address=address,
            sex=sex, medical=medical)
            messages.success(request, "Information Updated Successfully")
            return redirect('medical_info')
        else:
            med = MedicalInfo(user_id=c_id, first_name=first_name, last_name=last_name, email=email, phone=phone,
            dob=dob, blood_type=blood_type, m_status=m_status, address=address, sex=sex, medical=medical)
            med.save()
            messages.success(request, "Information Created Successfully")
            return redirect('medical_info')
    else:
        return render(request, 'medical.html')


# Logout Function that terminate the session of logged in User
def logout(request):
    s_logout(request)
    return redirect('signin')


# Funtion that display all the patient of the system
@login_required(login_url='signin')
def all_patient(request):
    show = MedicalInfo.objects.filter()
    return render(request, 'all_patient.html', {'show': show})


# Function that handle the filtering of User base on the healness
@login_required(login_url='signin')
def dropdown(request):
    if request.method == "POST":
        med = request.POST['med']
        ex = MedicalInfo.objects.filter(medical=med).exists()
        if ex:
            show = MedicalInfo.objects.filter(medical=med)
            messages.success(request, "Filtered Successfully")
            return render(request, 'dropdown.html', {'ex': ex, 'show': show})
        else:
            messages.error(request, "Data not found")
            return render(request, 'dropdown.html', {'ex': ex})
    else:
        return render(request, 'dropdown.html')


# Function that handle the chart
@login_required(login_url='signin')
def chart(request):

    malaria = MedicalInfo.objects.filter(medical="Malaria").count()
    Typhoid = MedicalInfo.objects.filter(medical="Typhoid").count()
    HighBP = MedicalInfo.objects.filter(medical="High BP").count()
    Diabetics = MedicalInfo.objects.filter(medical="Diabetics").count()
    # Chart data is passed to the `dataSource` parameter, as dictionary in the form of key-value pairs.
    dataSource = OrderedDict()

    # The `chartConfig` dict contains key-value pairs data for chart attribute
    chartConfig = OrderedDict()
    chartConfig["caption"] = "Statistical Details of the Medical Records"
    chartConfig["subCaption"] = ""
    chartConfig["xAxisName"] = "Healness"
    chartConfig["yAxisName"] = "Number of Patient"
    chartConfig["numberSuffix"] = ""
    chartConfig["theme"] = "fusion"

    # The `chartData` dict contains key-value pairs data
    chartData = OrderedDict()
    chartData["Malaria"] = malaria
    chartData["Typhoid"] = Typhoid
    chartData["High BP"] = HighBP
    chartData["Diabetics"] = Diabetics


    dataSource["chart"] = chartConfig
    dataSource["data"] = []

    # Convert the data in the `chartData` array into a format that can be consumed by FusionCharts.
    # The data for the chart should be in an array wherein each element of the array is a JSON object
    # having the `label` and `value` as keys.

    # Iterate through the data in `chartData` and insert in to the `dataSource['data']` list.
    for key, value in chartData.items():
        data = {}
        data["label"] = key
        data["value"] = value
        dataSource["data"].append(data)


    # Create an object for the column 2D chart using the FusionCharts class constructor
    # The chart data is passed to the `dataSource` parameter.
    column2D = FusionCharts("column2d", "ex1" , "1000", "600", "chart-1", "json", dataSource)
    return  render(request, 'chart.html', {'output' : column2D.render(), 'chartTitle': 'Simple Chart Using Array'})
