from django.urls import path
from . import views


urlpatterns = [
    path('signin', views.signin, name='signin'),
    path('signup', views.signup, name='signup'),
    path('doc_signup', views.doc_signup, name='doc_signup'),
    path('dashboard', views.dashboard, name='dashboard'),
    path('medical_info', views.medical_info, name='medical_info'),
    path('logout', views.logout, name='logout'),
    path('all_patient', views.all_patient, name='all_patient'),
    path('dropdown', views.dropdown, name='dropdown'),
    path('chart', views.chart, name='chart'),
]