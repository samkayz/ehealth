from django.shortcuts import render, redirect


def signin(request):
    return render(request, 'login.html')